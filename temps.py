from app.libraries.setup import Setup
from app.libraries.database import Database
from app.libraries.queries import Queries
from app.libraries.graphics import Graphics
from mastodon import Mastodon, StreamListener
from datetime import datetime
import unidecode
import time
import sys
import pdb

def run_alarms():    

    alarm_user, alarm_visibility, alarm_time, alarm_city = db.get_alarms()

    i = 0

    while i < len (alarm_user):

        print(current_time)

        alarmtime = datetime.strptime(alarm_time[i], "%H:%M").strftime("%H:%M")

        print(alarmtime)

        if alarmtime == current_time:

            username = alarm_user[i]

            visibility = alarm_visibility[i]

            city = alarm_city[i]

            alarm_post(username, visibility, city)
            
        i += 1

def alarm_post(username, visibility, city):
    
    data, people, long, lat = qry.get_weather_data(city)

    if data != None and 'error' not in data:

        days = 1

        graf = Graphics()

        detall, alt_text = graf.createtilehours(data, city)

        toot_text = f"@{username} \n"

        toot_text += f"El temps per hores a {city} \n"

        image_id = mastodon.media_post('images/tempsperhores12.png', "image/png", description=alt_text).id

        mastodon.status_post(toot_text, in_reply_to_id=None, visibility=visibility, media_ids={image_id})

    else:

        toot_text = f"@{username} \n"

        toot_text += f"No trobo {city} :-(\n"

        mastodon.status_post(toot_text, in_reply_to_id=status_id, visibility=visibility)

class Listener(StreamListener):

    def on_notification(self, notification):

        if notification.type != 'mention':

            return

        else:

            account_id = notification.account.id

            username = notification.account.acct

            status_id = notification.status.id

            text  = notification.status.content

            visibility = notification.status.visibility

            contingut = qry.cleanhtml(text)

            pregunta = qry.get_query(contingut)

            if unidecode.unidecode(pregunta)[0:9] == "consulta:":

                pais = qry.get_country(pregunta)

                inici = 0

                final = unidecode.unidecode(pregunta).index('consulta:',0)

                pregunta = pregunta.split(',')[0]

                if len(pregunta) > final :

                    pregunta = pregunta[0: inici:] + pregunta[final +10::]

                try:

                    pregunta = qry.format_query(pregunta)

                    data, people, long, lat = qry.get_weather_data(pregunta)

                    if data != None and 'error' not in data:

                        days = 1

                        graf = Graphics()

                        detall, alt_text = graf.createtiletoday(pregunta, people, long, lat, data, days)

                        toot_text = f'@{username} \n'

                        toot_text += f'El temps a {pregunta} és:\n'

                        toot_text += f'{detall}\n\n'

                        image_id = mastodon.media_post('images/temps.png', "image/png", description=f'{toot_text}\n{alt_text}').id 

                        mastodon.status_post(toot_text, in_reply_to_id=status_id, visibility=visibility, media_ids={image_id})

                    else:

                        toot_text = f'@{username} \n'

                        toot_text += f'No trobo {pregunta} :-(\n'

                        mastodon.status_post(toot_text, in_reply_to_id=status_id, visibility=visibility)

                except ValueError as verror:

                    country_error()

            elif unidecode.unidecode(pregunta)[0:5] == "avui:":

                pais = qry.get_country(pregunta)

                inici = 0

                final = unidecode.unidecode(pregunta).index('avui:',0)

                pregunta = pregunta.split(',')[0]

                if len(pregunta) > final :

                    pregunta = pregunta[0: inici:] + pregunta[final +6::]

                try:

                    pregunta = qry.format_query(pregunta)

                    data, people, long, lat = qry.get_weather_data(pregunta)

                    if data != None and 'error' not in data:

                        days = 1

                        graf = Graphics()

                        detall, alt_text = graf.createtilehours(data, pregunta)

                        toot_text = f"@{username} \n"

                        toot_text += f"El temps per hores a {pregunta} \n"

                        image_id = mastodon.media_post('images/tempsperhores12.png', "image/png", description=f'{toot_text}\n\n{alt_text}').id

                        mastodon.status_post(toot_text, in_reply_to_id=status_id, visibility=visibility, media_ids={image_id})

                    else:

                        toot_text = f"@{username} \n"

                        toot_text += f"No trobo {pregunta} :-(\n"

                        mastodon.status_post(toot_text, in_reply_to_id=status_id, visibility=visibility)

                except ValueError as verror:

                    country_error()

            elif unidecode.unidecode(pregunta)[0:8] == "setmana:":

                pais = qry.get_country(pregunta)

                inici = 0

                final = unidecode.unidecode(pregunta).index('setmana:',0)

                pregunta = pregunta.split(',')[0]

                if len(pregunta) > final :

                    pregunta = pregunta[0: inici:] + pregunta[final +9::]

                try:

                    pregunta = qry.format_query(pregunta)
              
                    data, people, long, lat = qry.get_weather_data(pregunta) 

                    if data != None and 'error' not in data:

                        days = 7

                        graf = Graphics()

                        detall, alt_text = graf.createtileweek(pregunta, data, days)

                        toot_text = f"@{username} \n"

                        toot_text += f"Previsió del temps de 7 dies a {pregunta} \n"

                        image_id = mastodon.media_post('images/tempsweek7.png', "image/png", description=alt_text).id

                        mastodon.status_post(toot_text, in_reply_to_id=status_id, visibility=visibility, media_ids={image_id})

                    else:

                        toot_text = f"@{username} \n"

                        toot_text += f"No trobo {pregunta} :-(\n"

                        mastodon.status_post(toot_text, in_reply_to_id=status_id, visibility=visibility)

                except ValueError as verror:

                    country_error()

            elif unidecode.unidecode(pregunta)[0:7] == "alarma:":

                inici = 0

                final = unidecode.unidecode(pregunta).index('alarma:',0)

                pregunta = pregunta.split(',')[0]

                if len(pregunta) > final :

                    pregunta = pregunta[0: inici:] + pregunta[final +8::]

                alarm_time = pregunta.split(' ')[0]

                alarm_city = pregunta[6:]

                if alarm_city != '':

                    db.set_alarm(username, visibility, alarm_time, alarm_city)

                    toot_text = f"@{username} \n"

                    toot_text += f"Alarma per a {alarm_city} fixada a les {alarm_time}\n"

                    mastodon.status_post(toot_text, in_reply_to_id=status_id, visibility=visibility)

                else:

                    toot_text = f"@{username} \n"

                    toot_text += f"Per a configurar una alarma cal fer-ho així:\n@temps alarma: 08:00 Ciutat"

                    mastodon.status_post(toot_text, in_reply_to_id=status_id, visibility=visibility)

            elif unidecode.unidecode(pregunta)[0:8] == "esborra:":

                inici = 0

                final = unidecode.unidecode(pregunta).index('esborra:',0)

                pregunta = pregunta.split(',')[0]

                if len(pregunta) > final :

                    pregunta = pregunta[0: inici:] + pregunta[final +9::]

                alarm_exists = db.check_alarm(username, pregunta)

                if alarm_exists:

                    db.delete_alarm(username, pregunta)

                    toot_text = f"@{username} \n"

                    toot_text += f"{pregunta}, alarma esborrada."

                    mastodon.status_post(toot_text, in_reply_to_id=status_id, visibility=visibility)

# main

if __name__ == '__main__':

    setup = Setup()

    db = Database()

    mastodon = Mastodon(
        access_token = setup.mastodon_app_token,
        api_base_url= setup.mastodon_hostname
        )

    t = time.localtime()

    current_time = time.strftime("%H:%M", t)

    qry = Queries()

    run_alarms()

    mastodon.stream_user(Listener())
