import os
import sys
import sqlite3
from sqlite3 import Error
import pdb

class Database():

    name = 'database library'

    def __init__(self, db=None):

        self.db = "app/database/database.db"

        if not os.path.isfile(self.db):

            if not os.path.isdir('app/database'):

                os.makedirs('app/database')

            self.__createdb(self)  

    def set_alarm(self, username, visibility, alarm_time, alarm_city):

        insert_sql = "INSERT INTO alarms(username, visibility, time, city) VALUES(?,?,?,?) ON CONFLICT DO NOTHING"

        conn = None

        try:

            conn = sqlite3.connect(self.db)

            cur = conn.cursor()

            cur.execute(insert_sql, (username, visibility, alarm_time, alarm_city))

            conn.commit()

            cur.close()

        except sqlite3.DatabaseError as db_error:

            print(db_error)

        finally:

            if conn is not None:

                conn.close()

    def get_alarms(self):

        alarm_user = []

        alarm_visibility = []

        alarm_time = []

        alarm_city = []

        conn = None

        try:

            conn = sqlite3.connect(self.db)

            cur = conn.cursor()

            cur.execute('select username, visibility, time, city from alarms')

            rows = cur.fetchall()

            for row in rows:

                alarm_user.append(row[0])

                alarm_visibility.append(row[1])

                alarm_time.append(row[2])

                alarm_city.append(row[3])
            
            cur.close()

        except sqlite3.DatabaseError as db_error:

            print(db_error)

        finally:

            if conn is not None:

                conn.close()
        
        return (alarm_user, alarm_visibility, alarm_time, alarm_city)

    def check_alarm(self, username, city):

        sql_query = 'select username, city from alarms where username=? and city=?'
        
        alarm_exists = False

        conn = None

        try:

            conn = sqlite3.connect(self.db)

            cur = conn.cursor()

            cur.execute(sql_query, (username, city))

            row = cur.fetchone()

            if row != None:

                alarm_exists = True

            cur.close()

        except sqlite3.DatabaseError as db_error:

            print(db_error)

        finally:

            if conn is not None:

                conn.close()
        
        return alarm_exists

    def delete_alarm(self, username, city):

        delete_sql = 'delete from alarms where username=? and city=?'

        conn = None

        try:

            conn = sqlite3.connect(self.db)

            cur = conn.cursor()

            cur.execute(delete_sql, (username, city))

            conn.commit()

            cur.close()

        except sqlite3.DatabaseError as db_error:

            print(db_error)

        finally:

            if conn is not None:

                conn.close()

    def create_index(self, table, index, sql_index):

      conn = None
      try:

        conn = sqlite3.connect(self.db)
        cur = conn.cursor()

        print(f"Creating index...{index}")
        # Create the table in sqlite3 database
        cur.execute(sql_index)

        conn.commit()

        print(f"Index {index} created!\n")

      except sqlite3.DatabaseError as db_error:

        print(db_error)

      finally:

        if conn is not None:

          conn.close()

    @staticmethod
    def __createdb(self):

        conn = None

        try:

            conn = sqlite3.connect(self.db)

            print(f"Database {self.db} created!\n")

            self.__dbtables_schemes(self)

        except sqlite3.DatabaseError as db_error:

            print(db_error)

        finally:

            if conn is not None:

                conn.close()

    @staticmethod
    def __dbtables_schemes(self):

        table = "alarms"
        sql = "create table "+table+" (username varchar(30), visibility varchar(8), time time, city varchar(30))"
        self.__create_table(self, table, sql)

        index = "alarm_pkey"
        sql_index = f'CREATE UNIQUE INDEX {index} ON {table} (username, city)'
        self.create_index(table, index, sql_index)

    @staticmethod
    def __create_table(self, table, sql):

        conn = None

        try:

            conn = sqlite3.connect(self.db)
            cur = conn.cursor()

            print(f"Creating table {table}")
            cur.execute(sql)

            conn.commit()
            print(f"Table {table} created!\n")

        except sqlite3.DatabaseError as db_error:

            print(db_error)

        finally:

            if conn is not None:

                conn.close()
