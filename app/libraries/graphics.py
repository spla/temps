import os
from datetime import timedelta, datetime
from PIL import Image, ImageFont, ImageDraw
import geotiler

def translate(i, detall, dir_vent):

    if detall == 'Parcialmente nuboso':
        detall = 'parcialment ennuvolat'
    if detall == 'Parcialmente nuboso con niebla':
        detall = 'parcialment ennuvolat amb boira'
    if detall == 'Parcialmente nuboso con probabilidad de lluvias':
        detall = 'parcialment ennuvolat amb possibles pluges'
    if detall == 'Parcialmente nuboso con lluvias':
        detall = 'parcialment ennuvolat amb aiguats'
    if detall == 'Nubes dispersas':
        detall = 'núvols dispersos'
    if detall == 'Muy nuboso con nevadas':
        detall = 'molt ennuvolat amb nevades'
    if detall == 'Muy nuboso con lluvia':
        detall = 'molt ennuvolat amb pluja'
    if detall == 'Muy nuboso con niebla':
        detall = 'molt ennuvolat amb boira'
    if detall == 'Muy nuboso con lluvias':
        detall = 'molt ennuvolat amb pluges'
    if detall == 'Cubierto':
        detall = 'tapat'
    if detall == 'Cubierto con lluvia':
        detall = 'tapat amb pluja'
    if detall == 'Cubierto con lluvias':
        detall = 'tapat amb pluges'
    if detall == 'Cubierto con tormentas':
        detall = 'tapat amb tempestes'
    if detall == 'Cubierto con probabilidad de lluvia':
        detall = 'tapat amb possible pluja'
    if detall == 'Cubierto con probabilidad de tormentas':
        detall = 'tapat amb possibles tempestes'
    if detall == 'Cubierto con probabilidad de lluvias':
        detall = 'tapat amb possibles pluges'
    if detall == 'Cubierto con nevadas':
        detall = 'tapat amb nevades'
    if detall == 'Muy nuboso':
        detall = 'molt ennuvolat'
    if detall == 'Muy nuboso con probabilidad de tormentas':
        detall = 'molt ennuvolat amb possibles tempestes'
    if detall == 'Muy nuboso con probabilidad de lluvias':
        detall = 'molt ennuvolat amb possibles pluges'
    if detall == 'Despejado':
        detall = 'clar'

    if dir_vent == 'Suroeste':
        dir_vent = 'sud-oest'
    if dir_vent == 'Noroeste':
        dir_vent = 'nord-oest'
    if dir_vent == 'Nordeste':
        dir_vent = 'nord-est'
    if dir_vent == 'Sureste':
        dir_vent = 'sud-est'
    if dir_vent == 'Norte':
        dir_vent = 'nord'
    if dir_vent == 'Este':
        dir_vent = 'est'
    if dir_vent == 'Sur':
        dir_vent = 'sud'
    if dir_vent == 'Oeste':
        dir_vent = 'oest'

    return i, detall, dir_vent

class Graphics():

    name = 'graphics library'

    def __init__(self):

        if not os.path.exists('images'):

                os.makedirs('images')

    def createtilehours(self, data, pregunta):

        alt_text = f'{pregunta}, 12 hores\n\n'

        i = 1
        x = 10
        y = 10

        while i < 13:

            hour_data = data['hour_hour']['hour'+str(i)]['hour_data']
            temperature = data['hour_hour']['hour'+str(i)]['temperature']
            detall = data['hour_hour']['hour'+str(i)]['text']
            icona = data['hour_hour']['hour'+str(i)]['icon']
            dir_vent = data['hour_hour']['hour'+str(i)]['wind_direction']

            i, detall, dir_vent = translate(i, detall, dir_vent)

            alt_text += f'{hour_data}: {temperature}°, {detall}, vent {dir_vent}.\n' 

            if i == 1:
                fons = Image.open('images/fonsorig.jpg')
            else:
                fons = Image.open('images/tempsperhores'+str(i-1)+'.png')

            if i == 1:

                logo_img = Image.open('images/logo.png')
                fons.paste(logo_img, (470, 16), logo_img)

            # hi afegim l'icona del temps
            icona_path = 'wi/'+icona+'.png'
            temps_img = Image.open(icona_path)

            fons.paste(temps_img, (x,y+30), temps_img)

            fons.save('images/temphour'+str(i)+'.png',"PNG")

            base = Image.open('images/temphour'+str(i)+'.png').convert('RGBA')
            txt = Image.new('RGBA', base.size, (255,255,255,0))
            fnt = ImageFont.truetype('fonts/DejaVuSerif.ttf', 25, layout_engine=ImageFont.Layout.BASIC)
            # get a drawing context
            draw = ImageDraw.Draw(txt)

            if i == 1:

                draw.text((10,10), f'{pregunta}, 12 hores', font=fnt, fill=(255,255,255,255)) ## half opacity

            fnt = ImageFont.truetype('fonts/DejaVuSerif.ttf', 18, layout_engine=ImageFont.Layout.BASIC)
            draw.text((x+50,y+45), f"{hour_data}: {temperature}\u00b0, {detall}", font=fnt, fill=(255,255,255,200)) ## half opacity

            if i == 1:

                fnt = ImageFont.truetype('fonts/DejaVuSerif.ttf', 12, layout_engine=ImageFont.Layout.BASIC)
                draw.text((380,490), 'temps@mastodont.cat', font=fnt, fill=(255,255,255,200)) #fill=(255,255,255,255)) ## full opacity
                draw.text((400,510), 'API: tutiempo.net', font=fnt, fill=(155,0,200,200)) #fill=(255,255,255,255)) ## full opacity 

            out = Image.alpha_composite(base, txt)
            out.save('images/tempsperhores'+str(i)+'.png')

            y = y + 40

            i += 1

        return detall, alt_text

    def createtiletoday(self, pregunta, people, long, lat, data, days):
  
        alt_text = ''

        i = 1

        while i < days+1:

            temperatura_max = data['day'+str(i)]['temperature_max']
            temperatura_min = data['day'+str(i)]['temperature_min']
            icona = data['day'+str(i)]['icon']
            detall = data['day'+str(i)]['text']
            vent = data['day'+str(i)]['wind']
            icona_vent = data['day'+str(i)]['icon_wind']
            dir_vent = data['day'+str(i)]['wind_direction']
            humitat = data['day'+str(i)]['humidity']
            solsurt = data['day'+str(i)]['sunrise']
            solamaga = data['day'+str(i)]['sunset']
            llunasurt = data['day'+str(i)]['moonrise']
            llunaamaga = data['day'+str(i)]['moonset']
            icona_lluna = data['day'+str(i)]['moon_phases_icon']

            i, detall, dir_vent = translate(i, detall, dir_vent)

            alt_text += f'temp. {temperatura_max}° / {temperatura_min}°\nvent {vent}km/h\nhumitat {humitat}%\nSol {solsurt}/{solamaga}\n'
            alt_text += f'Lluna {llunasurt}/{llunaamaga}'

            if people > 1000000:
                zoom = 10
            elif people > 100000 and people < 999999:
                zoom = 11
            elif people > 50000 and people < 99999:
                zoom = 12
            elif people < 49999:
                zoom = 15

            map = geotiler.Map(center=(long, lat), zoom=zoom, size=(256, 256))
            image = geotiler.render_map(map)
            image.save('images/map.png')

            fons = Image.open("images/fons.jpg")

            #hi afegim el mapa de la ciutat
            map_img = Image.open('images/map.png')
            fons.paste(map_img, (390,65), map_img)

            if i == 1:

                logo_img = Image.open('images/logo.png')
                fons.paste(logo_img, (310, 320), logo_img)
         
            # hi afegim l'icona del temps
            icona_path = 'wi/'+icona+'.png'
            temps_img = Image.open(icona_path)

            # hi afegim l'icona del termometre
            icona_temperature_path = 'temperatura/temperature.png'
            temperature_img = Image.open(icona_temperature_path)

            fons.paste(temperature_img, (10,115), temperature_img)

            # hi afegim l'icona del vent
            icona_vent_path = 'wd/'+icona_vent+'.png'
            vent_img = Image.open(icona_vent_path)

            fons.paste(vent_img, (22,180), vent_img)

            # hi afegim l'icona d'humitat
            icona_humitat_path = 'humitat/humedad.png'
            hum_img = Image.open(icona_humitat_path)

            fons.paste(hum_img, (8,210), hum_img)

            # hi afegim l'icona del sol sortint
            icona_sun_path = 'sol/solsurt.png'
            sun_img = Image.open(icona_sun_path)

            fons.paste(sun_img, (10,270), sun_img)

            # hi afegim l'icona de la lluna
            icona_lluna_path = 'canviats/'+icona_lluna+'.png'
            lluna_img = Image.open(icona_lluna_path)

            fons.paste(lluna_img, (12,315), lluna_img)
            fons.save('images/temporal.png',"PNG")

            base = Image.open('images/temporal.png').convert('RGBA')
            txt = Image.new('RGBA', base.size, (255,255,255,0))
            fnt = ImageFont.truetype('fonts/DejaVuSerif.ttf', 35, layout_engine=ImageFont.Layout.BASIC)
            # get a drawing context
            draw = ImageDraw.Draw(txt)

            draw.text((10,10), pregunta, font=fnt, fill=(255,255,255,255))

            fnt = ImageFont.truetype('fonts/DejaVuSerif.ttf', 30, layout_engine=ImageFont.Layout.BASIC)
            draw.text((70,60), detall, font=fnt, fill=(255,255,255,200)) ## half opacity
            draw.text((70,120), str(temperatura_max)+'\u00b0 / '+str(temperatura_min)+'\u00b0', font=fnt, fill=(255,255,255,200))
            draw.text((70,170), str(dir_vent)+', '+str(vent)+'km/h', font=fnt, fill=(255,255,255,200))
            draw.text((70,220), str(humitat)+'%', font=fnt, fill=(255,255,255,200))
            draw.text((70,270), str(solsurt)+', '+str(solamaga), font=fnt, fill=(255,255,255,200))
            draw.text((70,320), str(llunasurt)+', '+str(llunaamaga), font=fnt, fill=(255,255,255,200))

            if i == 1:

                fnt = ImageFont.truetype('fonts/DejaVuSerif.ttf', 12, layout_engine=ImageFont.Layout.BASIC)
                draw.text((380,350), 'temps@mastodont.cat', font=fnt, fill=(255,255,255,200)) #fill=(255,255,255,255)) ## full opacity
                draw.text((540,350), 'API: tutiempo.net', font=fnt, fill=(155,0,200,200)) #fill=(255,255,255,255)) ## full opacity 
                fnt = ImageFont.truetype('fonts/DejaVuSerif.ttf', 10, layout_engine=ImageFont.Layout.BASIC)
                draw.text((395,300),"© Col·laboradors d'OpenStreetMap, CC-BY-SA", font=fnt, fill=(155,0,200,200)) #fill=(255,255,255,255)) ## full opacity

            out = Image.alpha_composite(base, txt)
            out.save('images/temps.png')

            i += 1

        return detall, alt_text

    def createtileweek(self, pregunta, data, days):

        alt_text = f'{pregunta}, 7 dies\n\n'

        week_days= ['Dilluns', 'Dimarts', 'Dimecres', 'Dijous', 'Divendres', 'Dissabte','Diumenge']

        x = 10
        y = 10

        i = 1
        while i < days+1:

            weather_date = data['day'+str(i)]['date']
            temperatura_max = data['day'+str(i)]['temperature_max']
            temperatura_min = data['day'+str(i)]['temperature_min']
            icona = data['day'+str(i)]['icon']
            detall = data['day'+str(i)]['text']
            vent = data['day'+str(i)]['wind']
            icona_vent = data['day'+str(i)]['icon_wind']
            dir_vent = data['day'+str(i)]['wind_direction']
            humitat = data['day'+str(i)]['humidity']
            solsurt = data['day'+str(i)]['sunrise']
            solamaga = data['day'+str(i)]['sunset']
            llunasurt = data['day'+str(i)]['moonrise']
            llunaamaga = data['day'+str(i)]['moonset']
            icona_lluna = data['day'+str(i)]['moon_phases_icon']

            i, detall, dir_vent = translate(i, detall, dir_vent)

            alt_text += f'{weather_date}: {detall}, temp {temperatura_max}° / {temperatura_min}°.\n'

            date_list = list(map(str, weather_date. split('-')))
            day=datetime(int(date_list[0]),int(date_list[1]),int(date_list[2])).weekday()

            if i == 1:
                fons = Image.open('images/fons.jpg')
            else:
                fons = Image.open('images/tempsweek'+str(i-1)+'.png')

            # hi afegim l'icona del temps
            icona_path = 'wi/'+icona+'.png'
            temps_img = Image.open(icona_path)

            fons.paste(temps_img, (y+10, x+80), temps_img)

            if i == 1:
                logo_img = Image.open('images/logo.png')
                fons.paste(logo_img, (15, 320), logo_img)

            fons.save('images/tempweek'+str(i)+'.png',"PNG")

            base = Image.open('images/tempweek'+str(i)+'.png').convert('RGBA')
            txt = Image.new('RGBA', base.size, (255,255,255,0))
            fnt = ImageFont.truetype('fonts/DejaVuSerif.ttf', 35, layout_engine=ImageFont.Layout.BASIC)
            # get a drawing context
            draw = ImageDraw.Draw(txt)

            if i == 1:

                draw.text((15,10), f'{pregunta}, 7 dies', font=fnt, fill=(255,255,255,255))

            fnt = ImageFont.truetype('fonts/DejaVuSerif.ttf', 16, layout_engine=ImageFont.Layout.BASIC)

            if i == 1:
                draw.text((y+10,x+50), "Avui", font=fnt, fill=(255,255,255,220)) #fill=(255,255,255,255)) ## full opacity
            elif week_days[day] == 'Dimarts':
                draw.text((y+4,x+50), str(week_days[day]), font=fnt, fill=(255,255,255,220)) #fill=(255,255,255,255)) ## full opacity
            elif week_days[day] == 'Dimecres':
                draw.text((y-5,x+50), str(week_days[day]), font=fnt, fill=(255,255,255,220)) #fill=(255,255,255,255)) ## full opacity
            elif week_days[day] == 'Dijous':
                draw.text((y+6,x+50), str(week_days[day]), font=fnt, fill=(255,255,255,220)) #fill=(255,255,255,255)) ## full opacity
            elif week_days[day] == 'Divendres':
                draw.text((y-6,x+50), str(week_days[day]), font=fnt, fill=(255,255,255,220)) #fill=(255,255,255,255)) ## full opacity
            elif week_days[day] == 'Dissabte':
                draw.text((y,x+50), str(week_days[day]), font=fnt, fill=(255,255,255,220)) #fill=(255,255,255,255)) ## full opacity
            else:
                draw.text((y+4,x+50), str(week_days[day]), font=fnt, fill=(255,255,255,220)) #fill=(255,255,255,255)) ## full opacity

            fnt = ImageFont.truetype('fonts/DejaVuSerif.ttf', 24, layout_engine=ImageFont.Layout.BASIC)
            draw.text((y+18,x+140), str(temperatura_max)+'\u00b0', font=fnt, fill=(255,255,255,220)) #fill=(255,255,255,255)) ## full opacity
            draw.text((y+20,x+190), str(temperatura_min)+'\u00b0', font=fnt, fill=(255,255,255,220)) #fill=(255,255,255,255)) ## full opacity

            if i == 1:

                fnt = ImageFont.truetype('fonts/DejaVuSerif.ttf', 15, layout_engine=ImageFont.Layout.BASIC)
                draw.text((60,330), 'temps@mastodont.cat - 2020', font=fnt, fill=(255,255,255,200)) #fill=(255,255,255,255)) ## full opacity
                draw.text((520,330), 'API: tutiempo.net', font=fnt, fill=(155,0,200,200)) #fill=(255,255,255,255)) ## full opacity

            out = Image.alpha_composite(base, txt)
            out.save('images/tempsweek'+str(i)+'.png')

            y += 95
            i += 1

        return detall, alt_text
