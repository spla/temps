# temps, bot climatològic per al fedivers.

El codi de @temps@mastodont.cat! Aquest codi es un bot per a Mastodon que retorna en format gràfic el temps que fa o farà en qualsevol poble o ciutat que se li demani.  
Qualsevol usuari de qualsevol servidor del fedivers pot consultar el clima:  

@bot consulta: Barcelona  
@bot avui: Barcelona  
@bot setmana: Barcelona  
  
El bot respondrà amb format gràfic l'informació climatològica obtinguda de la API de tutiempo.net (cal una API key) i amb l'ajut de OpenStreetMap mostrarà el mapa de la zona consultada.  
També se li pot demanar que ens publiqui el temps a una hora determinada (alarma) amb:  

@bot alarma: 08:00 Barcelona  

O esborra l'alarma amb:  

@bot esborra: Barcelona  

### Dependències

-   **Python 3**
-   Servidor Postgresql
-   API key de tutiempo.net

## Instal·lació  

1. Clona aquest repositori: git clone https://git.mastodont.cat/spla/temps.git <directori_desti>  

2. Entra dins del <directori_desti> i crea l'Entorn Virtual de Python: `python3.x -m venv .`  

3. Activa l'Entorn Virtual de Python: `source bin/activate`  

4. Executa `pip install -r requirements.txt` per a afegir les llibreries necessàries.  

5. Executa `python setup.py` per a configurar el bot de Mastodon, els seus tokens d'accés.  

6. Executa `python db-setup.py` per a crear la base de dades necessaria per les alarmes.  

7. Executa `python apikey-setup.py` per a configurar la API key obtinguda a tutiempo.net.  
   
8. Emprea el teu mètode favorit de programació de tasques (per exemple cron) per a que s'executi `python temps.py` cada minut. 
