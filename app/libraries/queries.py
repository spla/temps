import os
import re
import requests
import urllib3
import getpass
from geopy.geocoders import Nominatim
import geocoder
import pdb

urllib3.disable_warnings()

class Queries():

    name = 'queries library'

    def __init__(self, apikey_filepath=None, api_url=None, apikey=None, contingut=None, pregunta=None):

        self.apikey_filepath = "secrets/apikey.txt"
        self.api_url = 'https://api.tutiempo.net/json/'
        self.apikey = apikey
        self.contingut = contingut
        self.pregunta = pregunta

        is_setup = self.__check_apikey_setup(self)

        if is_setup:

            self.apikey = self.__get_apikey_parameter("api_key", self.apikey_filepath)

        else:

            self.apikey = self.apikey_setup(self)

    @staticmethod
    def __check_apikey_setup(self):

        is_setup = False

        if not os.path.isfile(self.apikey_filepath):
            print(f"File {self.apikey_filepath} not found, running setup.")
        else:
            is_setup = True

        return is_setup

    @staticmethod
    def apikey_setup(self):

        if not os.path.exists('secrets'):
            os.makedirs('secrets')

        self.apikey = getpass.getpass("Enter tutiempo.net's API key: ")

        if not os.path.exists(self.apikey_filepath):
            with open(self.apikey_filepath, 'w'): pass
            print(f"{self.apikey_filepath} created!")

        with open(self.apikey_filepath, 'a') as the_file:
            print("Writing API key to " + self.apikey_filepath)
            the_file.write(f'api_key: {self.apikey}')

        return self.apikey

    def __get_apikey_parameter(self, parameter, apikey_filepath):

        if not os.path.isfile(apikey_filepath):
            print(f"File {apikey_filepath} not found..")

        with open( self.apikey_filepath ) as f:
            for line in f:
                if line.startswith( parameter ):
                    return line.replace(parameter + ":", "").strip()

    def cleanhtml(self, raw_html):

        cleanr = re.compile('<.*?>')
        cleantext = re.sub(cleanr, '', raw_html)
        return cleantext

    def get_query(self, contingut):

        inici = contingut.index("@")

        final = contingut.index(" ")

        if len(contingut) > final:

            contingut = contingut[0: inici:] + contingut[final +1::]

        neteja = contingut.count('@')

        i = 0
        while i < neteja :

            inici = contingut.rfind("@")
            final = len(contingut)
            contingut = contingut[0: inici:] + contingut[final +1::]
            i += 1

        pregunta = contingut.lstrip(" ")

        return pregunta

    def format_query(self, pregunta):

        if pregunta.find("'") != -1:

            pregunta = pregunta.replace('&apos;', "'")

        elif pregunta.find("’") != -1:

            pregunta = pregunta.replace("’", "'")

        elif pregunta.find("&apos;") != -1:

            pregunta = pregunta.replace('&apos;', "'")

        elif pregunta.find("&#39;") != -1:

            pregunta = pregunta.replace('&#39;', "'")

        return pregunta

    def get_weather_data(self, pregunta):

        nom = Nominatim(user_agent="temps@mastodont.cat")
        coords = nom.geocode(pregunta)

        if coords != None:

            long = coords.longitude
            lat  = coords.latitude

            headers = {"User-Agent":"Mozilla/5.0 (Macintosh; Intel Mac OS X 12_0_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36"}
        
            g = geocoder.geonames(pregunta, key='spla')

            people = g.population if g.ok else 0
        
            #url = f"{self.api_url}?lan=es&apid={}&ll={},{}".format(self.api_key, lat, long)

            url = f"{self.api_url}?lan=es&apid={self.apikey}&ll={lat},{long}"

            res = requests.get(url, headers=headers, verify=False)

            data = res.json()

        else:

            data = None
            people = None
            long = None
            lat = None
        
        return (data, people, long, lat)

    def get_country(self, pregunta):

        if pregunta.find(',') != -1:

            pais = pregunta.rsplit(',', 1)[1]
            pais = pais.replace(' ', '')
            pais = pais.upper()

        else:

            pais = 'ES'

        return pais






