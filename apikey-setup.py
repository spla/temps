#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pdb
import getpass
import fileinput,re  
import os
import sys

def create_dir():
  if not os.path.exists('secrets'):
    os.makedirs('secrets')

def create_file():
  if not os.path.exists('secrets/apikey.txt'):
    with open('secrets/apikey.txt', 'w'): pass
    print(secrets_filepath + " created!") 

def write_params():
  with open(secrets_filepath, 'a') as the_file:
    print("Writing  api key parameter to " + secrets_filepath)
    the_file.write('api_key: \n')

def modify_file(file_name,pattern,value=""):  
    fh=fileinput.input(file_name,inplace=True)  
    for line in fh:  
        replacement=pattern + value  
        line=re.sub(pattern,replacement,line)  
        sys.stdout.write(line)  
    fh.close()  

def get_parameter( parameter, file_path ):

    # Check if secrets file exists
    if not os.path.isfile(file_path):
        print("File %s not found, creating it."%file_path)
        create_dir()
        create_file()
        write_params()
        api_key = input('Enter your API key: ')
        modify_file(file_path, "api_key: ", value=api_key)

    # Find parameter in file
    with open( file_path ) as f:
        for line in f:
            if line.startswith( parameter ):
                return line.replace(parameter + ":", "").strip()

    # Cannot find parameter, exit
    print(file_path + " Missing parameter %s "%parameter)
    sys.exit(0)

# main

if __name__ == '__main__':


    # Load secrets from secrets file
    secrets_filepath = "secrets/apikey.txt"
    api_key = get_parameter("api_key", secrets_filepath)

